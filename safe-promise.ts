type _ = unknown;

export class Effect<A> {
  static raise = <A>(
    promise: Promise<A>,
    continuation?: ((fault: _) => A) | A
  ) => new Effect<A>(promise, continuation);

  constructor(
    readonly promise: Promise<A>,
    readonly continuation?: ((fault: _) => A) | A
  ) {}
}

export class Success<A> {
  constructor(readonly value: A) {}
}

export class Fault {
  constructor(readonly reason: Error | string | _) {}
}

export type Result<A> = Fault | Success<A>;

export type Computation<A = _, B = A, C = A> = Generator<
  Effect<A> | A,
  B | void,
  C
>;

export default async function interpret<A = _, B = A>(
  init: (raise: typeof Effect.raise) => Computation<A, B, _>
): Promise<Result<_>> {
  const generator = init(Effect.raise);
  let value: A | null = null;
  let item = generator.next();

  while (!item.done) {
    if (item.value instanceof Effect) {
      const { promise, continuation } = item.value;
      try {
        value = await promise;
      } catch (reason) {
        if (continuation == null) return new Fault(reason);
        if (continuation instanceof Function) value = continuation(reason);
        else value = continuation;
      }
    } else {
      value = item.value;
    }
    item = generator.next(value);
  }

  generator.return();
  return new Success(value);
}

function* G(raise: typeof Effect.raise): Computation<number> {
  const x = yield raise(Promise.resolve(42));
  const y = yield raise(Promise.resolve(x + 8));
  const z = yield raise(Promise.reject(Error("42")), y + 12);
  console.log(z);
}

interpret(G).then(console.log);
